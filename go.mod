module gitlab.com/aknudsen/example-go-rest-api

go 1.12

require (
	github.com/dgraph-io/badger v1.6.0
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.3
	github.com/rs/zerolog v1.14.3
	github.com/spf13/cobra v0.0.5
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80 // indirect
	golang.org/x/sys v0.0.0-20190804053845-51ab0e2deafa // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190809145639-6d4652c779c4 // indirect
	golang.org/x/xerrors v0.0.0-20190717185122-a985d3407aa7
	gotest.tools v2.2.0+incompatible
)
