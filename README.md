# Example Go REST API
An example of a Go REST API.

We use github.com/gorilla/mux for request routing, as well as github.com/spf13/cobra for argument
parsing, otherwise the server is implemented using the standard library.

This example API is centered around the *post* resource, each of which has a title and content.
The API uses JSON for content encoding. The following endpoints exist:

* GET /posts => Get a list of all posts.
* POST /posts => Add a post.
* GET /posts/{id} => Get a certain post, using its identifier.
* PATCH /posts/{id} => Edit a certain post, using its identifier, sending a document of changes 
  to make.
* DELETE /posts/{id} = Delete a certain post, using its identifier.
