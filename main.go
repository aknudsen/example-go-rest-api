package main

import (
	"context"
	"encoding/json"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"time"

	"golang.org/x/xerrors"

	"github.com/dgraph-io/badger"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
)

var badRequestError = xerrors.New("bad request")
var notFoundError = xerrors.New("not found")

// Post represents a post.
type Post struct {
	Id      string `json:"id"`
	Title   string `json:"title"`
	Content string `json:"content"`
}

// Server implements the API.
type Server struct {
	db *badger.DB
}

func (s Server) getPostsHandler(w http.ResponseWriter, r *http.Request) error {
	log.Debug().Msg("Received request to get posts")
	if _, err := w.Write([]byte("[")); err != nil {
		return xerrors.Errorf("failed writing to response: %w", err)
	}

	if err := s.db.View(func(txn *badger.Txn) error {
		iter := txn.NewIterator(badger.DefaultIteratorOptions)
		defer iter.Close()

		i := 0
		for iter.Rewind(); iter.Valid(); iter.Next() {
			if i > 0 {

				if _, err := w.Write([]byte(",")); err != nil {
					return xerrors.Errorf("failed to write to response: %w", err)
				}
			}
			i++

			item := iter.Item()
			err := item.Value(func(postB []byte) error {
				if _, err := w.Write(postB); err != nil {
					return xerrors.Errorf("failed to write post to response: %w", err)
				}

				return nil
			})
			if err != nil {
				return err
			}
		}
		return nil
	}); err != nil {
		return err
	}

	if _, err := w.Write([]byte("]")); err != nil {
		return xerrors.Errorf("failed writing to response: %w", err)
	}

	return nil
}

func (s Server) addPostHandler(w http.ResponseWriter, r *http.Request) error {
	log.Debug().Msg("Received request to add post")
	decoder := json.NewDecoder(r.Body)
	var post Post
	if err := decoder.Decode(&post); err != nil {
		log.Debug().Err(err).Msg("Failed to decode post object from request")
		return xerrors.Errorf("badly encoded input: %w", badRequestError)
	}

	log.Debug().Str("postTitle", post.Title).Msg("Successfully decoded post object from request")
	post.Title = strings.TrimSpace(post.Title)
	if post.Title == "" {
		return xerrors.Errorf("title cannot be empty: %w", badRequestError)
	}
	post.Content = strings.TrimSpace(post.Content)
	if post.Content == "" {
		return xerrors.Errorf("content cannot be empty: %w", badRequestError)
	}

	if err := s.db.Update(func(txn *badger.Txn) error {
		id, err := uuid.NewUUID()
		if err != nil {
			return err
		}
		idB, err := id.MarshalText()
		if err != nil {
			return err
		}
		post.Id = string(idB)

		marshaled, err := json.Marshal(post)
		if err != nil {
			return err
		}

		if err := txn.Set(idB, marshaled); err != nil {
			return err
		}

		log.Debug().Str("postId", string(idB)).Msg("Successfully added post")
		if _, err := w.Write(marshaled); err != nil {
			return err
		}

		return nil
	}); err != nil {
		return err
	}

	return nil
}

func (s Server) getPostHandler(w http.ResponseWriter, r *http.Request) error {
	vars := mux.Vars(r)
	idS := vars["id"]
	log.Debug().Str("id", idS).Msg("Received request to get post")

	if err := s.db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(idS))
		if err != nil {
			if err == badger.ErrKeyNotFound {
				log.Debug().Str("postId", idS).Msg("Post not found in database")
				return xerrors.Errorf("post not found in database: %w", notFoundError)
			}

			log.Error().Err(err).Str("postId", idS).Msg("Failed to get post from database")
			return xerrors.Errorf("failed to get post from database: %w", err)
		}

		if err := item.Value(func(val []byte) error {
			_, err := w.Write(val)
			if err != nil {
				return xerrors.Errorf("failed to write post to response: %w", err)
			}

			return nil
		}); err != nil {
			return err
		}

		return nil
	}); err != nil {
		return err
	}

	return nil
}

func (s Server) editPostHandler(w http.ResponseWriter, r *http.Request) error {
	vars := mux.Vars(r)
	idS := vars["id"]
	log.Debug().Str("postId", idS).Msg("Received request to edit post")
	var input map[string]string
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&input); err != nil {
		return err
	}

	log.Debug().Str("postId", idS).Interface("input", input).Msg("Editing patch")
	if err := s.db.Update(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(idS))
		if err != nil {
			return xerrors.Errorf("failed to get post from database: %w", notFoundError)
		}

		if err := item.Value(func(val []byte) error {
			var post Post
			if err := json.Unmarshal(val, &post); err != nil {
				return xerrors.Errorf("failed to decode post JSON: %w", err)
			}
			if title, ok := input["title"]; ok {
				log.Debug().Msg("Setting post title")
				post.Title = title
			} else {
				log.Debug().Msg("Not setting post title")
			}
			if content, ok := input["content"]; ok {
				log.Debug().Msg("Setting post content")
				post.Content = content
			} else {
				log.Debug().Msg("Not setting post content")
			}
			marshaled, err := json.Marshal(post)
			if err != nil {
				return xerrors.Errorf("failed to encode post as JSON: %w", err)
			}
			if err := txn.Set([]byte(idS), marshaled); err != nil {
				return xerrors.Errorf("failed to write edited post back to database: %w", err)
			}

			return nil
		}); err != nil {
			return err
		}

		return nil
	}); err != nil {
		return err
	}

	return nil
}

func (s Server) deletePostHandler(w http.ResponseWriter, r *http.Request) error {
	vars := mux.Vars(r)
	idS := vars["id"]
	log.Debug().Str("postId", idS).Msg("Received request to delete post")

	if err := s.db.Update(func(txn *badger.Txn) error {
		if err := txn.Delete([]byte(idS)); err != nil {
			log.Error().Err(err).Str("postId", idS).Msg("Failed to delete post from database")
			return xerrors.Errorf("failed to delete post from database: %w", err)
		}

		log.Debug().Str("postId", idS).Msg("Successfully deleted post")
		return nil
	}); err != nil {
		return err
	}

	return nil
}

func jsonMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("content-type", "application/json")
		next.ServeHTTP(w, r)
	})
}

func makeHandler(handler func(http.ResponseWriter, *http.Request) error) func(
	http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := handler(w, r); err != nil {
			if xerrors.Is(err, notFoundError) {
				log.Debug().Err(err).Msg("Handler returned not found")
				http.Error(w, err.Error(), http.StatusNotFound)
				return
			}
			if xerrors.Is(err, badRequestError) {
				log.Debug().Err(err).Msg("Handler returned bad request")
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}

			log.Error().Err(err).Msg("Handler returned an error")
			http.Error(w, "Internal server error", http.StatusInternalServerError)
		}
	}
}

func getHealthzHandler(w http.ResponseWriter, r *http.Request) error {
	return nil
}

var wait time.Duration
var dbPath string

func execute(cmd *cobra.Command, args []string) error {
	db, err := badger.Open(badger.DefaultOptions(dbPath))
	if err != nil {
		return err
	}

	defer db.Close()
	server := Server{
		db: db,
	}

	r := mux.NewRouter()
	r.Use(jsonMiddleware)
	r.HandleFunc("/healthz", makeHandler(getHealthzHandler)).Methods("GET")
	r.HandleFunc("/posts", makeHandler(server.getPostsHandler)).Methods("GET")
	r.HandleFunc("/posts", makeHandler(server.addPostHandler)).Methods("POST").Headers(
		"content-type", "application/json")
	r.HandleFunc("/posts/{id}", makeHandler(server.getPostHandler)).Methods("GET")
	r.HandleFunc("/posts/{id}", makeHandler(server.editPostHandler)).Methods("PATCH")
	r.HandleFunc("/posts/{id}", makeHandler(server.deletePostHandler)).Methods("DELETE")

	srv := &http.Server{
		Addr: "0.0.0.0:8080",
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r,
	}

	log.Info().Msg("Starting server...")

	errChan := make(chan error, 1)

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			if err != http.ErrServerClosed {
				errChan <- err
			}
		}
	}()

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	select {
	case <-sigChan:
		break
	case err := <-errChan:
		log.Error().Err(err).Msg("Server returned an error")
		return err
	}

	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()

	log.Info().Msg("Shutting down...")
	if err := srv.Shutdown(ctx); err != nil {
		return err
	}

	return nil
}

func main() {
	log.Logger = log.With().Caller().Logger()

	rootCmd := &cobra.Command{
		Use:          "example-go-rest-api",
		Short:        "An example REST API server written in Go",
		RunE:         execute,
		SilenceUsage: true,
	}

	rootCmd.PersistentFlags().DurationVarP(&wait, "timeout", "t", time.Second*10,
		"timeout for graceful shutdown - e.g. 15s or 1m")
	rootCmd.PersistentFlags().StringVarP(&dbPath, "db-path", "d", "db/",
		"path of database directory")

	if err := rootCmd.Execute(); err != nil {
		log.Fatal().Err(err).Msg("")
	}
}
