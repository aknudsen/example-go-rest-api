package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"os/exec"
	"syscall"
	"testing"
	"time"

	"gotest.tools/assert"
)

const port = 8080

func startServer(t *testing.T) *exec.Cmd {
	cmd := exec.Command("go", "run", "main.go", "-d", "tmp")
	// Give child process its own group so that we can kill it, and its children, in isolation
	// from ourselves
	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	err := cmd.Start()
	assert.NilError(t, err)

	var i int
	for i = 0; i < 10; i++ {
		resp, err := http.Get(fmt.Sprintf("http://localhost:%d/healthz", port))
		if err == nil && resp.StatusCode == 200 {
			break
		}

		time.Sleep(1 * time.Second)
	}
	assert.Assert(t, i < 10, "Failed to start server")

	return cmd
}

func stopServer(t *testing.T, cmd *exec.Cmd) {
	// Kill process and its children by sending signal to its group (indicated by the negative
	// of the PID)
	err := syscall.Kill(-cmd.Process.Pid, syscall.SIGKILL)
	assert.NilError(t, err)
	_ = cmd.Wait()
	_ = os.RemoveAll("tmp/")
}

func TestGetPosts(t *testing.T) {
	cmd := startServer(t)
	defer stopServer(t, cmd)

	post := Post{Title: "Title", Content: "Content"}
	postB, err := json.Marshal(post)
	assert.NilError(t, err)

	resp, err := http.Post(fmt.Sprintf("http://localhost:%d/posts", port), "application/json",
		bytes.NewBuffer(postB))
	assert.NilError(t, err)
	assert.Equal(t, resp.StatusCode, 200)
	decoder := json.NewDecoder(resp.Body)
	var addedPost Post
	err = decoder.Decode(&addedPost)
	assert.NilError(t, err)
	assert.Assert(t, addedPost.Id != "")
	assert.Equal(t, addedPost.Title, post.Title)
	assert.Equal(t, addedPost.Content, post.Content)

	resp, err = http.Get(fmt.Sprintf("http://localhost:%d/posts", port))
	assert.NilError(t, err)
	assert.Equal(t, resp.StatusCode, 200)

	var gotPosts []Post
	decoder = json.NewDecoder(resp.Body)
	err = decoder.Decode(&gotPosts)
	assert.NilError(t, err)
	assert.DeepEqual(t, gotPosts, []Post{addedPost})
}

func TestGetPost(t *testing.T) {
	cmd := startServer(t)
	defer stopServer(t, cmd)

	post := Post{Title: "Title", Content: "Content"}
	postB, err := json.Marshal(post)
	assert.NilError(t, err)
	resp, err := http.Post(fmt.Sprintf("http://localhost:%d/posts", port), "application/json",
		bytes.NewBuffer(postB))
	assert.NilError(t, err)
	assert.Equal(t, resp.StatusCode, 200)
	decoder := json.NewDecoder(resp.Body)
	var addedPost Post
	err = decoder.Decode(&addedPost)
	assert.NilError(t, err)
	assert.Assert(t, addedPost.Id != "")
	assert.Equal(t, addedPost.Title, post.Title)
	assert.Equal(t, addedPost.Content, post.Content)

	resp, err = http.Get(fmt.Sprintf("http://localhost:%d/posts/%s", port, addedPost.Id))
	assert.NilError(t, err)
	assert.Equal(t, resp.StatusCode, 200)

	var gotPost Post
	decoder = json.NewDecoder(resp.Body)
	err = decoder.Decode(&gotPost)
	assert.NilError(t, err)
	assert.DeepEqual(t, gotPost, addedPost)
}

func TestGetNonExistentPost(t *testing.T) {
	cmd := startServer(t)
	defer stopServer(t, cmd)

	resp, err := http.Get(fmt.Sprintf("http://localhost:%d/posts/%s", port, "non-existent"))
	assert.NilError(t, err)
	assert.Equal(t, resp.StatusCode, 404)
}

func TestAddPost(t *testing.T) {
	cmd := startServer(t)
	defer stopServer(t, cmd)

	post := Post{Title: "Title", Content: "Content"}
	postB, err := json.Marshal(post)
	assert.NilError(t, err)

	resp, err := http.Post(fmt.Sprintf("http://localhost:%d/posts", port), "application/json",
		bytes.NewBuffer(postB))
	assert.NilError(t, err)
	assert.Equal(t, resp.StatusCode, 200)

	decoder := json.NewDecoder(resp.Body)
	var addedPost Post
	err = decoder.Decode(&addedPost)
	assert.NilError(t, err)
	assert.Assert(t, addedPost.Id != "")
	assert.Equal(t, addedPost.Title, post.Title)
	assert.Equal(t, addedPost.Content, post.Content)
}

func TestDeletePost(t *testing.T) {
	cmd := startServer(t)
	defer stopServer(t, cmd)

	post := Post{Title: "Title", Content: "Content"}
	postB, err := json.Marshal(post)
	assert.NilError(t, err)

	resp, err := http.Post(fmt.Sprintf("http://localhost:%d/posts", port), "application/json",
		bytes.NewBuffer(postB))
	assert.NilError(t, err)
	assert.Equal(t, resp.StatusCode, 200)
	decoder := json.NewDecoder(resp.Body)
	var addedPost Post
	err = decoder.Decode(&addedPost)
	assert.NilError(t, err)
	assert.Assert(t, addedPost.Id != "")
	assert.Equal(t, addedPost.Title, post.Title)
	assert.Equal(t, addedPost.Content, post.Content)

	client := &http.Client{}
	req, err := http.NewRequest("DELETE",
		fmt.Sprintf("http://localhost:%d/posts/%s", port, addedPost.Id), nil)
	assert.NilError(t, err)
	resp, err = client.Do(req)
	assert.NilError(t, err)
	assert.Equal(t, resp.StatusCode, 200)

	resp, err = http.Get(fmt.Sprintf("http://localhost:%d/posts/%s", port, addedPost.Id))
	assert.NilError(t, err)
	assert.Equal(t, resp.StatusCode, 404)
}

func TestDeleteNonExistentPost(t *testing.T) {
	cmd := startServer(t)
	defer stopServer(t, cmd)

	client := &http.Client{}
	req, err := http.NewRequest("DELETE",
		fmt.Sprintf("http://localhost:%d/posts/%s", port, "non-existent"), nil)
	assert.NilError(t, err)
	resp, err := client.Do(req)
	assert.NilError(t, err)
	assert.Equal(t, resp.StatusCode, 200)
}

func TestEditPost(t *testing.T) {
	cmd := startServer(t)
	defer stopServer(t, cmd)

	post := Post{Title: "Title", Content: "Content"}
	postB, err := json.Marshal(post)
	assert.NilError(t, err)

	resp, err := http.Post(fmt.Sprintf("http://localhost:%d/posts", port), "application/json",
		bytes.NewBuffer(postB))
	assert.NilError(t, err)
	assert.Equal(t, resp.StatusCode, 200)
	decoder := json.NewDecoder(resp.Body)
	var addedPost Post
	err = decoder.Decode(&addedPost)
	assert.NilError(t, err)
	assert.Assert(t, addedPost.Id != "")
	assert.Equal(t, addedPost.Title, post.Title)
	assert.Equal(t, addedPost.Content, post.Content)

	post.Title = "Edited Title"
	post.Content = "Edited Content"
	postB, err = json.Marshal(post)
	assert.NilError(t, err)
	client := &http.Client{}
	req, err := http.NewRequest("PATCH",
		fmt.Sprintf("http://localhost:%d/posts/%s", port, addedPost.Id), bytes.NewBuffer(postB))
	assert.NilError(t, err)
	resp, err = client.Do(req)
	assert.NilError(t, err)
	assert.Equal(t, resp.StatusCode, 200)

	resp, err = http.Get(fmt.Sprintf("http://localhost:%d/posts/%s", port, addedPost.Id))
	assert.NilError(t, err)
	assert.Equal(t, resp.StatusCode, 200)
	decoder = json.NewDecoder(resp.Body)
	var gotPost Post
	err = decoder.Decode(&gotPost)
	assert.NilError(t, err)

	assert.DeepEqual(t, gotPost, Post{
		Id:      addedPost.Id,
		Title:   "Edited Title",
		Content: "Edited Content",
	})
}

func TestEditNonExistentPost(t *testing.T) {
	cmd := startServer(t)
	defer stopServer(t, cmd)

	post := Post{Title: "Title", Content: "Content"}
	postB, err := json.Marshal(post)
	assert.NilError(t, err)
	client := &http.Client{}
	req, err := http.NewRequest("PATCH",
		fmt.Sprintf("http://localhost:%d/posts/%s", port, "non-existent"), bytes.NewBuffer(postB))
	assert.NilError(t, err)
	resp, err := client.Do(req)
	assert.NilError(t, err)
	assert.Equal(t, resp.StatusCode, 404)
}
